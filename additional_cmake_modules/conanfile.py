#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans.model.conan_file import ConanFile
from conans import tools


class CMakeModules(ConanFile):
    name = "additional_cmake_modules"
    version = "20190212"
    license = "BSL-1.0"
    url = "https://bitbucket.org/headcount_bimandscan/conan-additional-cmake-modules"
    description = "Ryan Pavlik's and Lars Bilke's bundle of CMake modules."
    generators = "txt"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    homepage = "https://github.com/bilke/cmake-modules"
    no_copy_source = True

    _src_dir = "additional_cmake_modules_src"

    exports = "../LICENCE.md"

    def source(self):
        git_uri = "https://github.com/bilke/cmake-modules.git"

        git = tools.Git(folder = self._src_dir)
        git.clone(url = git_uri,
                  branch = "master")
        git.checkout(element = "9d5f5fa551826cf071b898dce9b8b2f27df0bcd5") # commit hash -> 12/2/2019

    def build(self):
        pass

    def package(self):
        self.copy(pattern = "*",
                  src = os.path.join(self._src_dir,
                                     "."),
                  dst = "cmake/additional",
                  keep_path = True)

        self.copy("LICENSE_1_0.txt",
                  "licenses",
                  self._src_dir)

    def package_id(self):
        self.info.header_only()
