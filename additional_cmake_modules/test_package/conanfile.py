#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 © BIM & Scan® Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans.model.conan_file import ConanFile

from conans import CMake, \
                   tools


class PkgTest_AdditionalCMakeModules(ConanFile):
    name = "pkgtest_additional_cmake_modules"
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    build_requires = "additional_cmake_modules/20190212@bimandscan/unstable"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def imports(self):
        pass

    def test(self):
        os.chdir("bin")
        self.run(f".{os.sep}{self.name}")
