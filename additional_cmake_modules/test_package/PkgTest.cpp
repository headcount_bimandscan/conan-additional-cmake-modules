/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'Additional CMake Modules' package test (compilation, linking, and execution).\n";

    std::cout << "This executable build was configured with the additional CMake modules.\n";

    std::cout << "'Additional CMake Modules' package works!" << std::endl;
    return EXIT_SUCCESS;
}
