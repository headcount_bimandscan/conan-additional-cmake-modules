# BIM & Scan� Third-Party Library (Additional CMake Modules)

![BIM & Scan](BimAndScan.png "BIM & Scan� Ltd.")

Conan build script for Ryan Pavlik's and Lars Bilke's bundle of [CMake modules](https://github.com/bilke/cmake-modules).

Supports snapshot 12/2/2019 (unstable, from Git repo).

Installs all scripts/modules into the 'cmake/additional' folder for access from other projects.

To use (in CMakeLists.txt):

```CMake
list(APPEND CMAKE_MODULE_PATH "${CONAN_ADDITIONAL_CMAKE_MODULES_ROOT}/cmake/additional")
```

...then include the desired modules.

You might want to also enable backported CMake support via:

```CMake
include(UseBackportedModules)
```

