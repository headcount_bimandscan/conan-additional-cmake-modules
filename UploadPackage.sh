#!/bin/sh

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#

set -e
conan upload -c -r "bimandscan-public" --all "additional_cmake_modules/20190212@bimandscan/unstable"
